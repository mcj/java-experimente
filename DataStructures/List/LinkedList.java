public class LinkedList<T> implements List<T> {
    private Node<T> head;

    @Override
    public LinkedListIterator<T> getBegin() {
        LinkedListIterator<T> iter = new LinkedListIterator<>(this.head);
        return iter;
    }

    @Override
    public LinkedListIterator<T> getEnd() {
        return new LinkedListIterator<T>(null);
    }

    @Override
    public void prepend(T newItem) {
        head = new Node<T>(newItem, head);
    }

    @Override
    public void insertAfter(ListIterator<T> iter, T newItem) {
        LinkedListIterator<T> iter2 = (LinkedListIterator<T>) iter;
        iter2.node.next = new Node<T>(newItem, iter2.node.next);
    }

    @Override
    public T removeFirst() {
        Node<T> prevHead = head;
        head = head.next;
        return prevHead.val;
    }

    @Override
    public T removeAfter(ListIterator<T> iter) {
        LinkedListIterator<T> iter2 = (LinkedListIterator<T>) iter;
        Node<T> toDelete = iter2.node.next;

        iter2.node.next = iter2.node.next.next;

        return toDelete.val;
    }

    @Override
    public boolean isEmpty() {
        return (head == null);
    }

    @Override
    public int getSize() {
        int size;

        Node<T> n = head;
        for (size = 0; n != null; size++)
            n = n.next;

        return size;
    }

    @Override
    public T getNth(int nth) throws Exception {
        return findNth(nth).val;
    }

    @Override
    public void setNth(int nth, T newItem) throws Exception {
        findNth(nth).val = newItem;
    }

    private Node<T> findNth(int nth) throws Exception {
        if (nth >= this.getSize())
            throw new Exception("The index is past the end of the list");

        int i;

        Node<T> node = head;
        for (i = 0; i < nth; i++) {
            node = node.next;
        }

        return node;
    }

    public class LinkedListIterator
        <T> implements ListIterator<T> {
        protected Node<T> node;

        public LinkedListIterator(Node<T> node) {
            this.node = node;
        }

        public T get() {
            return (node != null)? node.val : null;
        }

        public void set(T newItem) {
            node.val = newItem;
        }

        public void next() {
            node = node.next;
        }
    }

    protected class Node<T> {
        T val;
        Node<T> next;

        Node(T val, Node<T> next) {
            this.val = val;
            this.next = next;
        }
    }
}
