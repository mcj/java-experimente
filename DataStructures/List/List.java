public interface List<T> {
    /**
     * return an Iterator for the beginning of this list
     */
    public ListIterator<T> getBegin();

    /**
     * return an Iterator for the end of this list
     */
    public ListIterator<T> getEnd();

    /**
     * Add a new element to the beginning of the list
     */
    public void prepend(T newItem);

    /**
     * insert an item after iter
     */
    public void insertAfter(ListIterator<T> iter, T newItem);

    /**
     * remove the first element in the list
     */
    public T removeFirst();

    /**
     * remove the element after iter
     */
    public T removeAfter(ListIterator<T> iter);

    /**
     * whether the list is empty
     */
    public boolean isEmpty();

    /**
     * return the size of the list
     */
    public int getSize();

    /**
     * return the nth element of this list
     */
    public T getNth(int n) throws Exception;

    /**
     * set the nth element of this list
     */
    public void setNth(int n, T newItem) throws Exception;
}
