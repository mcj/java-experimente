public class ListTest {
    public static void main(String[] args) {
        LinkedList<String> list = new LinkedList<>();

        list.prepend("The beginning");
        list.insertAfter(list.getBegin(), "The end");
        list.insertAfter(list.getBegin(), "in the middle");

        list.prepend("nonsense");
        list.removeFirst();

        System.out.println("Is empty? " + list.isEmpty());
        System.out.println("Length: " + list.getSize());

        try {
            System.out.println("First: " + list.getNth(0));
            list.setNth(1, "in between");
        } catch (Exception ex) {
            System.err.println("Whoops... something went wrong");
        }

        System.out.println();
        System.out.println("All list items:");

        ListIterator<String> it = list.getBegin();
        String s;
        while ((s = it.get()) != null) {
            System.out.println(" " + s);
            it.next();
        }
    }
}
