public interface ListIterator<T> {
    /**
     * return the current list item
     */
    public T get();
    /**
     * set the current list item
     */
    public void set(T newItem);
    /**
     * move to the next list item and return it
     */
    public void next();
}
