import java.nio.file.*;
import java.nio.*;
import java.io.*;
import java.util.*;
import javax.swing.*;

import static java.nio.file.StandardWatchEventKinds.*;

public class Dateien extends JFrame {
    private Path pfad;
    private Path wurzel;
    private JList<Object> liste;

    public Dateien(String pfadName) {
        wurzel = Paths.get("/");

        try {
            pfad = wurzel.resolve(pfadName);
        } catch (InvalidPathException ex) {
            System.out.println("Pfad existiert nicht");
            System.exit(2);
        }

        liste = new JList<>();

        aktualisieren();

        add(liste);
        setSize(400, 500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    private void aktualisieren() {
        ArrayList<Object> l = new ArrayList<>();

        try {
            java.util.stream.Stream<Path> s = Files.list(pfad);
            s.forEach(p -> { l.add(p); p = null; System.gc();  });
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace();
            System.exit(3);
        }

        liste.setListData(l.toArray());

        System.out.println("Aktualisiert");
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Aufruf: java Dateien <pfad>");
            System.exit(1);
        }

        Dateien dateien = new Dateien(args[0]);

        try (WatchService waechter = FileSystems.getDefault().newWatchService()) {
            dateien.pfad.register(waechter, ENTRY_CREATE, ENTRY_DELETE);

            for(;;) {
                WatchKey schluessel = waechter.take();

                schluessel.pollEvents();

                dateien.aktualisieren();

                schluessel.reset();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(4);
        }
    }
}
