import java.nio.file.*;
import java.nio.*;
import java.io.*;

import static java.nio.file.StandardWatchEventKinds.*;

public class Dateien2 {

    private static void dateienAnzeigen(File pfad) {
        String[] liste = pfad.list();

        for (String s : liste) {
            if (!s.startsWith(".") && !s.endsWith("~"))
                System.out.println(s);
        }
        System.out.println("---------------------");
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Aufruf: java Dateien <pfad>");
            System.exit(1);
        }

        File pfad = new File(args[0]);

        if (!pfad.exists()) {
            System.err.println("Datei existiert nicht");
            System.exit(1);
        }

        if (!pfad.isDirectory()) {
            System.err.println("Ist kein Verzeichnis");
            System.exit(2);
        }

        dateienAnzeigen(pfad);

        try (WatchService waechter = FileSystems.getDefault().newWatchService()) {
            Path p = Paths.get(pfad.toURI());
            p.register(waechter, ENTRY_CREATE, ENTRY_DELETE);

            for (int i = 0; i < 3; i++) {
                WatchKey schluessel = waechter.take();

                dateienAnzeigen(pfad);

                schluessel.pollEvents();

                if (!schluessel.reset()) {
                    System.out.println(schluessel);
                    break;
                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(3);
        }
    }
}
