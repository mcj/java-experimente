import java.nio.*;
import java.nio.file.*;

import static java.nio.file.StandardWatchEventKinds.*;

public class PfadBeobachten {
    public static void main(String[] args) {
        Path pfad = Paths.get("/home/mc/Java/");

        try (WatchService waechter = FileSystems.getDefault().newWatchService()) {
            pfad.register(waechter, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);

            for(;;) {
                WatchKey schluessel = waechter.take();

                for (WatchEvent<?> event: schluessel.pollEvents()) {
                    System.out.println(event);
                    System.out.println(event.kind());
                }

                schluessel.reset();
            }
        } catch (InterruptedException ex) {
            return;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return;
    }
}
