@FunctionalInterface
    interface LambdaInterface {
    void doSomething();
}

public class LambdaTest {
    public static void main(String[] args) {
        LambdaInterface lambda = () ->
            System.out.println("Hallo Lambdas!");

        lambda.doSomething();

        Comparable<Object> comp = (x) -> {
            System.out.println("In Comparable");
            return 1;
        };

        comp.compareTo(null);

        new Thread(() -> {
                    for (int i = 0; i < 10000; i++)
                        if (i % 100 == 0)
                            System.out.print("X");
        }).start();

        new Thread(() -> {
                    for (int i = 0; i < 10000; i++)
                        if (i % 100 == 0)
                            System.out.print(".");
        }).start();

        System.out.println();
    }
}
