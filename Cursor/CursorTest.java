import javax.swing.*;
import java.awt.event.*;

import java.awt.Cursor;

/**
 * Cycles through all available mouse cursor types.  Useful for
 * testing new cursor designs
 */
public class CursorTest extends JFrame {
    public CursorTest() {
        super("Cursor Test");

        int[] cursors = {
            Cursor.CROSSHAIR_CURSOR,
            Cursor.E_RESIZE_CURSOR,
            Cursor.HAND_CURSOR,
            Cursor.MOVE_CURSOR,
            Cursor.N_RESIZE_CURSOR,
            Cursor.NE_RESIZE_CURSOR,
            Cursor.NW_RESIZE_CURSOR,
            Cursor.S_RESIZE_CURSOR,
            Cursor.SE_RESIZE_CURSOR,
            Cursor.SW_RESIZE_CURSOR,
            Cursor.TEXT_CURSOR,
            Cursor.W_RESIZE_CURSOR,
            Cursor.WAIT_CURSOR
        };

        JButton mainPanel = new JButton();
        JLabel cursorName = new JLabel("");

        mainPanel.addActionListener(new ActionListener() {
                int i = 0;

                public void actionPerformed(ActionEvent e) {
                    Cursor newCursor;
                    try {
                        newCursor = new Cursor(cursors[i++]);
                    } catch (Exception ex) {
                        i = 0;
                        newCursor = new Cursor(cursors[i++]);
                    }

                    mainPanel.setCursor(newCursor);
                    cursorName.setText(newCursor.getName());
                }
            });

        this.add(mainPanel);
        this.add(cursorName, "South");
        this.setSize(500, 400);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        new CursorTest().setVisible(true);
    }
}
