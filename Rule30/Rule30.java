import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.util.Random;

public class Rule30 extends JPanel implements ComponentListener {
    private BufferedImage buffer;
    private Rule rule;
    private boolean randomizeFirstRow;
    private int seed;

    public Rule30(Rule rule, boolean randomize, int seed) {
        this.rule = rule;
        this.randomizeFirstRow = randomize;
        this.seed = seed;
        buffer = new BufferedImage(300, 100, BufferedImage.TYPE_3BYTE_BGR);

        addComponentListener(this);
    }

    public static void main(String[] args) throws NumberFormatException {
        int ruleNr = Integer.parseInt(args[0]);

        boolean rand = false;
        if (args.length > 1 && args[1].equals("true"))
            rand = true;

        int seed = (int) System.currentTimeMillis();
        if (args.length > 2)
            seed = Integer.parseInt(args[2]);

        JPanel p = new Rule30(new Rule(ruleNr), rand, seed);
        JFrame window = new JFrame("Rule " + ruleNr);
        window.add(p);
        window.setSize(1000, 500);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setVisible(true);
    }

    @Override
    public void paintComponent(Graphics g) {
        paintBuffer();
        g.drawImage(buffer, 0, 0, this);
    }

    @Override
    public void componentResized(ComponentEvent e) {
        resetBuffer();
        invalidate();
    }

    private void resetBuffer() {
        if (this.getWidth() > 0 && this.getHeight() > 0)
            buffer = new BufferedImage(getWidth(), getHeight(),
                    BufferedImage.TYPE_3BYTE_BGR);

        buffer.getGraphics().setColor(new Color(255, 255, 255));
        buffer.getGraphics().fillRect(0, 0, buffer.getWidth(), buffer.getHeight());
    }

    private void paintBuffer() {
        int black = 0x000000;
        int white = 0xffffff;

        boolean[][] array = computeArray();
        for (int y = 0; y < array.length; y++)
            for (int x = 0; x < array[y].length; x++)
                buffer.setRGB(x, y, array[y][x] ? black : white);
    }

    private boolean[][] computeArray() {
        boolean[][] array = initializeArray();

        for (int currRow = 1; currRow < buffer.getHeight(); currRow++) {
            int lastRow = currRow - 1;

            for (int x = 0; x < buffer.getWidth() - 2; x++) {
                boolean left = array[lastRow][x];
                boolean middle = array[lastRow][x + 1];
                boolean right = array[lastRow][x + 2];

                boolean nextval = rule.computeCellColor(left, middle, right);
                array[currRow][x + 1] = nextval;
            }
        }

        return array;
    }

    private boolean[][] initializeArray() {
        boolean[][] array = new boolean[buffer.getHeight()][buffer.getWidth()];

        if (randomizeFirstRow) {
            Random rand = new Random(seed);

            for (int x = 0; x < buffer.getWidth(); x++)
                array[0][x] = rand.nextBoolean();
        } else {
            int centerCell = buffer.getWidth() / 2;
            array[0][centerCell] = true;
        }

        return array;
    }

    public void componentHidden(ComponentEvent e) {}
    public void componentMoved(ComponentEvent e) {}
    public void componentShown(ComponentEvent e) {}
}

class Rule {
    private int rule;

    public Rule(int rule) {
        this.rule = rule;
    }

    public boolean computeCellColor(int left, int middle, int right) {
        return computeCellColor(left > 0, middle > 0, right > 0);
    }

    public boolean computeCellColor(boolean left, boolean middle, boolean right) {
        int idx = (left ? 1 : 0)*4 + (middle ? 1 : 0)*2 + (right ? 1 : 0);
        return computeCellColor(idx);
    }

    public boolean computeCellColor(int idx) {
        return (rule & (1 << idx)) > 0;
    }
}
