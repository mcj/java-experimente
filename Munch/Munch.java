// DATAI 2
// ADDB 1,2
// ROTC 2,-22
// XOR 1,2
// JRST .-4

// XScreenSaver
// https://en.wikipedia.org/wiki/Munching_square
// https://en.wikipedia.org/wiki/HAKMEM

import javax.swing.*;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class Munch extends JPanel implements Runnable {
   private final int SIZE = 256;
   private final int DELAY = 17;
   
   private int n;
   private int rgb;
   private BufferedImage buffer;
   
   public Munch() {
      java.util.Random random = new java.util.Random();
      rgb = random.nextInt(0x1000000);
      
      buffer = new BufferedImage(SIZE, SIZE, BufferedImage.TYPE_3BYTE_BGR); 

      new Thread(this).start();
   }

    public void run() {
        try {
            n = 0;

            while (true) {
                if (n == SIZE) {
                    n = 0;

                    java.util.Random random = new java.util.Random();
                    rgb = random.nextInt(0x1000000);
                }

                for(int x = 0; x < SIZE; x++) {
                    for(int y = 0; y < SIZE; y++) {
                        if ((x ^ y) == n) {
                            buffer.setRGB(x, y, rgb);
                        }
                    }
                }

                n++;
                repaint();
                Thread.sleep(DELAY);
            }
        } catch (InterruptedException ex) { }
   }

   public void paintComponent(Graphics g) {
      g.drawImage(buffer, 0, 0, this);
   }

   public Dimension getPreferredSize() {
      return new Dimension(SIZE, SIZE);
   }

   public static void main(String[] args) {
      JFrame fenster = new JFrame();
      fenster.add(new Munch());
      fenster.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      fenster.pack();
      fenster.setVisible(true);
   }
}
