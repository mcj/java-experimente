import javax.swing.*;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class Munch3 extends JPanel implements Runnable {
    private final int SIZE = 512;
    private final int DELAY = 17;
   
    private int n;
    private int[] farben;
    private BufferedImage buffer;
   
    public Munch3() {
        farben = new int[SIZE];

        buffer = new BufferedImage(SIZE, SIZE, BufferedImage.TYPE_3BYTE_BGR); 

        new Thread(this).start();
    }

    public void run() {
        java.util.Random zufall = new java.util.Random();

        try {
            n = SIZE;

            while (true) {
                if (n == SIZE) {
                    n = 0;

                    int mul = 1 << (zufall.nextInt(3) * 8);
                    for (int i = 0; i < farben.length; i++)
                        farben[i] = 0x000000 + mul * i / (farben.length / 256);
                }

                for(int x = 0; x < SIZE; x++) {
                    for(int y = 0; y < SIZE; y++) {
                        if ((x ^ y) == n) {
                            buffer.setRGB(x, y, farben[n] ^ buffer.getRGB(x, y));
                        }
                    }
                }

                n++;
                repaint();
                Thread.sleep(DELAY);
            }
        } catch (InterruptedException ex) { }
    }

    public void paintComponent(Graphics g) {
        g.drawImage(buffer, 0, 0, this);
    }

    public Dimension getPreferredSize() {
        return new Dimension(SIZE, SIZE);
    }

    public static void main(String[] args) {
        JFrame fenster = new JFrame();
        fenster.add(new Munch2());
        fenster.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        fenster.pack();
        fenster.setVisible(true);
    }
}
