import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class Langton extends JComponent implements Runnable {
    final int breite = 100;
    final int hoehe = 70;
    boolean[][] felder = new boolean[breite][hoehe];
    int antX;
    int antY;
    int richtung = 0x00;

    public Langton() {
        antX = breite/2;
        antY = hoehe/2;

        Random z = new Random();
        for (int i = breite/3; i < breite-(breite/3); i++)
            for (int j = hoehe/3; j < hoehe-(hoehe/3); j++) {
                // set all:
                felder[i][j] = true;
                // or set randomly:
                //z.nextInt(2) == 0? true : false;
            }
    }

    public void run() {
        try {
            while (true) {
                if (felder[antX][antY] == false)
                    richtung = (richtung + 1) & 3;
                else
                    richtung = (richtung - 1) & 3;

                felder[antX][antY] = ! felder[antX][antY];

                if (richtung == 0)
                    antY = --antY >= 0? antY : hoehe-1;
                else if (richtung == 1)
                    antX = ++antX < breite? antX : 0;
                else if (richtung == 2)
                    antY = ++antY < hoehe? antY : 0;
                else if (richtung == 3)
                    antX = --antX >= 0? antX : breite-1;

                repaint();
                Thread.sleep(20);
            }
        } catch (InterruptedException ex) {
        }
    }

    public void paintComponent(Graphics g) {
        for (int i = 0; i < breite; i++)
            for (int j = 0; j < hoehe; j++) {
                g.setColor(felder[i][j]? Color.BLACK : Color.WHITE);
                g.fillRect(i*10, j*10, 10, 10);
            }

        g.setColor(Color.RED);
        g.fillOval(antX*10+1, antY*10+1, 8, 8);
    }

    public Dimension getPreferredSize() {
        return new Dimension(breite*10, hoehe*10);
    }

    public static void main(String[] args) {
        JFrame fenster = new JFrame("Langton's Ant");
        Langton l = new Langton();
        fenster.add(l);
        fenster.pack();
        fenster.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        fenster.setVisible(true);

        new Thread(l).start();
    }
}
