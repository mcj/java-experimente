import javax.swing.*;
import java.awt.*;
import java.awt.image.*;

public class Test extends JPanel implements Runnable {
    double winkel;
    double r;
    BufferedImage puffer;
    Graphics2D g;

    Test() {
    }

    public void run() {
        int x, y;
        winkel = 0;
        r = 0;

        puffer = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_3BYTE_BGR);
        g = puffer.createGraphics();
        g.setStroke(new BasicStroke(4));
        g.translate(getWidth() / 2, getHeight() / 2);
        g.setColor(Color.RED);

        for(int i = 0; i < 800; i++) {
            x = (int) -(Math.sin(winkel)*r);
            y = (int) (Math.cos(winkel)*r);

            g.drawLine(0, 0, x, y);

            repaint();

            winkel+=0.2;
            r++;

            try{ Thread.sleep(30); }
            catch(InterruptedException e){ return; }
        }
    }

    public void paintComponent(Graphics g) {
        g.drawImage(puffer, 0, 0, this);
    }

    public static void main(String[] args) {
        JFrame fenster = new JFrame();
        Test t = new Test();
        fenster.add(t, "Center");
        fenster.setSize(600, 630);
        fenster.setDefaultCloseOperation(3);
        fenster.setVisible(true);

        new Thread(t).start();
    }
}
