public class BubbleSort {
    public static void bubblesort(int[] arr) {
        boolean sorted = false;

        while (! sorted) {
            sorted = true;
            for (int j = 0; j < arr.length - 1; j++) {
                if (arr[j] < arr[j+1]) {
                    int tmp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = tmp;
                    sorted = false;
                }
            }
        }
    }

    public static void main(String[] args) {
        int[] array = { 1, 3, 5, 9, 2, 5, 28, 195, 0, 14598 };

        bubblesort(array);

        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
    }
}
