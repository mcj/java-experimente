import java.util.*;

public class Philosopher implements Runnable {
    private Fork[] forks;
    protected List<Fork> forksInUse = new ArrayList<>();
    private String name;

    public Philosopher(String name, Fork[] forks) {
        this.name = name;
        this.forks = forks;
    }

    public void run() {
        try {
            while (true) {
                think();
                eat();
            }
        } catch (InterruptedException ex) {
            return;
        }
    }

    public void think() throws InterruptedException {
        System.out.println(name + ": Thinking...");
        doNothingForAWhile();
    }

    public void eat() throws InterruptedException {
        System.out.println(name + ": I'm hungry");

        while (! haveAllForks()) {
            for (Fork f : forks) {
                if (f.pickUp()) {
                    forksInUse.add(f);
                } else {
                    System.out.println(name + ": can't get fork");
                    putDownAllForks();
                    Thread.sleep(50);
                }
            }
        }

        System.out.println(name + ": Eating");
        doNothingForAWhile();
        putDownAllForks();
        System.out.println(name + ": done with eating");
    }

    private boolean haveAllForks() {
        return forksInUse.size() == forks.length;
    }

    private void doNothingForAWhile() throws InterruptedException {
        Thread.sleep((int) (Math.random() * 2000));
    }

    public void putDownAllForks() {
        for (Fork f : forksInUse) {
            f.putDown();
        }
        forksInUse.clear();
    }
}
