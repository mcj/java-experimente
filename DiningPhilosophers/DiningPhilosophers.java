public class DiningPhilosophers {
    public static void main(String[] args) {
        final int NUM_THINKERS = 2;
        final String[] names = new String[] {
            "Andy",
            "Phil",
            "Claire",
            "Ian",
            "Mary",
            "Ann",
            "Robert",
            "Jamie",
            "Jenny",
            "Katharina"
        };

        Philosopher[] thinkers = new Philosopher[NUM_THINKERS];

        Fork firstFork = new Fork();
        Fork fork1 = firstFork;
        Fork fork2;
        for (int i = 0; i < NUM_THINKERS; i++) {
            if (i == NUM_THINKERS - 1 && NUM_THINKERS != 1)
                fork2 = firstFork;
            else
                fork2 = new Fork();

            thinkers[i] = new Philosopher(names[i], new Fork[] {fork1, fork2});
            fork1 = fork2;
        }

        for (Philosopher p : thinkers)
            new Thread(p).start();
    }
}
