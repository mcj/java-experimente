public class Fork {
    private boolean inUse = false;

    public boolean isInUse() {
        return inUse;
    }

    public synchronized boolean pickUp() {
        if (! inUse) {
            inUse = true;
            return true;
        } else {
            return false;
        }
    }

    public void putDown() {
        inUse = false;
    }
}
