import javax.swing.*;

public class CubicProgressTest extends JFrame {
    public static void main(String[] args) {
        new CubicProgressTest();
    }

    public CubicProgressTest() {
        super("Cubic Process Bars Test");

        CubicProgressBar b1 = new CubicProgressBar();
        b1.setMaximum(50);
        b1.setString("0/50");
        b1.setStringPainted(true);
        CubicProgressBar b2 = new CubicProgressBar();
        b2.setMaximum(200);
        b2.setString("0/200");
        b2.setStringPainted(true);

        this.setLayout(new java.awt.FlowLayout());
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(500, 400);

        this.add(b1);
        this.add(b2);

        this.setVisible(true);

        new Thread(() -> {
                    int i = 0;

                    while (true) {
                        try { Thread.sleep(500); }
                        catch (InterruptedException ex) { }

                        b1.setValue(i);
                        b1.setString(i + "/50");
                        b2.setValue(i);
                        b2.setString(i + "/200");

                        System.out.println(i++);
                    }
        }).start();
    }
}
