import javax.swing.JProgressBar;

/**
 * progress bar that increases speed
 */
public class CubicProgressBar extends JProgressBar {
    private double cubicProcess(double prog) {
        // return 4/9 x³ + 5/9 x² + 0 x + 0
        return ((4.0 / 9.0) * prog * prog * prog) + ((5.0 / 9.0) * prog * prog);
    }

    public void setValue(int n) {
        double newValue;

        double procent = this.getMaximum() / 100.0;
        double nProcent = n / procent / 100.0;
        newValue = cubicProcess(nProcent) * 100.0 * procent;

        super.setValue((int) Math.round(newValue));
    }
}
